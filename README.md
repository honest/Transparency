
# Transparency
This repo contains my public PGP key, and updates on projects I'm working on. Always be sure to verify my updates with my PGP key. Git allows signing commits with a public key, which will show up as a green banner on the commit (on Gitdab). If you're really unsure, you can verify the message with my key, but it's probably not neccessary.
